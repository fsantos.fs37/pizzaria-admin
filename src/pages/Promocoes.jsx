import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import * as styles from '../StyledComponents/Select';
import accounting from '../utils/formatMoney';

class Promocoes extends Component {
    state = {
        combo: [],
        selected: [],
        name: '',
        value: '',
        numeroSabores: 1,
        tipo: 'pizzasCombo',
    }

    componentDidMount() {
        const { getComboProducts } = this.props.ProductsStore;
        const { getPromo, promo } = this.props.PromoStore;
        getComboProducts();
        console.log(promo)
        getPromo()

    }

    selectHandler = produto => {
        const { selected, combo } = this.state;
        if (selected.includes(produto.id)) {
            selected.splice(selected.indexOf(produto.id), 1);
            combo.splice(combo.indexOf(produto));
            return this.setState({ selected, combo }, _ => console.log(this.state));
        }
        if (selected.length === 2) return;
        selected.push(produto.id);
        combo.push({ ...produto });
        this.setState({ selected, combo }, _ => console.log(this.state));
    }

    cadastrarCombo = _ => {
        if (this.state.combo.length < 2) return alert('Escolha os produtos da promoção');
        const res = window.confirm('Você deseja cadastrar este novo combo?')
        if (res) {
            const combo = this.state.combo.map(item => {
                return { ...item, tipo: 'promocao', value: 0 }
            })
            const { cadastrarPromo } = this.props.PromoStore;
            cadastrarPromo({ combo: combo })
        }
    }

    render() {
        const { ComboProducts: { pizzas } } = this.props.ProductsStore;
        const { promo } = this.props.PromoStore;

        const selecteds = promo.map(pizza => <styles.Container key={pizza.id}>
            <styles.Option
                selected>
                <styles.NamePrice>
                    <span>{pizza.name}</span> <span>{accounting.formatMoney(pizza.value)}</span>
                </styles.NamePrice>
                <styles.NamePrice>
                    Tamanho: <span>{pizza.size}</span>
                </styles.NamePrice>
                <styles.Ingredients>{pizza.ingredients}</styles.Ingredients>
            </styles.Option >
        </styles.Container >)


        const pizzasContainer = pizzas && pizzas.map(pizza => {
            return (
                pizza.tipo !== 'pizzasCombo' &&
                <styles.Container key={pizza.id}>
                    <styles.Option
                        onClick={_ => this.selectHandler(pizza)}
                        selected={this.state.selected.includes(pizza.id)}>
                        <styles.NamePrice>
                            <span>{pizza.name}</span> <span>{accounting.formatMoney(pizza.value)}</span>
                        </styles.NamePrice>
                        <styles.NamePrice>
                            Tamanho: <span>{pizza.size}</span>
                        </styles.NamePrice>
                        <styles.Ingredients>{pizza.ingredients}</styles.Ingredients>
                    </styles.Option >
                </styles.Container >
            )
        })

        return (
            <section className='admin-section' >
                <h1 className='section-title'>Promoções</h1>
                <styles.MainContainer>
                    <styles.Ul>
                        <h2> Atuais</h2>
                        {selecteds}</styles.Ul>
                    <styles.Ul>
                        <h2>Pizzas</h2>
                        {pizzasContainer}</styles.Ul>
                    <styles.Button onClick={this.cadastrarCombo}>Cadastrar opções</styles.Button>
                </styles.MainContainer>

            </section>
        )

    }
}

export default inject('PromoStore', 'ProductsStore')(observer(Promocoes));