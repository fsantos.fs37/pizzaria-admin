import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import '../styles/Users.css'

class Recentes extends Component {
    componentDidMount() {
        const { getUsers } = this.props.UserStore;
        getUsers();
    }

    render() {
        const { users } = this.props.UserStore;

        const userCards = users.map(item => {
            const { user, pedidosArray, adress } = item;

            console.log(user)
            return (
                <div className="user-card">
                    <div className="user-profile-photo">
                        <img src={user.photoURL} alt="" />
                    </div>
                    <div className="user-name">
                        <p>{user.name}</p>
                    </div>
                    <div className="user-tel">
                        {adress ? <p>Telefone: {adress.tel.value}</p> : null}
                    </div>
                    <div className="user-tel">
                        <p>Email: {user.email}</p>
                    </div>
                    <div className="user-selos">
                        {pedidosArray ? <p>Selos: {pedidosArray.length} </p> : null}
                    </div>
                </div>
            )
        })

        return (
            <section className='admin-section'>
                <h2 className='section-title'>Usuários</h2>
                {userCards}
                <div className='users-container'>

                </div>
            </section>
        )
    }
}

export default inject('UserStore')(observer(Recentes));

