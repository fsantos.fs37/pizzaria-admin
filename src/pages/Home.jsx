import React, { Component } from 'react';

// import { auth, database } from './firebase/firebase';

class Home extends Component {
    render() {
        return (
            <section className='admin-section'>
                <div className='admin-intro-message'>
                    <p>
                        Bem vindo a seção do administrador. Faça Login para continar.
                    </p>
                </div>
            </section>
        )
    }
}

export default Home;
