import React, { Component } from 'react';
import Edit from '../components/Edit';

class Editar extends Component {
    render() {
        return (
            <section className='admin-section'>
                <h2 className='section-title'>Editar produto</h2>
                <Edit />
            </section>
        )
    }
}

export default Editar;
