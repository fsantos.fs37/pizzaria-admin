import React, { Component } from 'react';
import InformacoesForm from '../components/InformacoesForm';

class Informacoes extends Component {
    render() {
        return (
            <section className='admin-section'>
                <h2 className='section-title'>Informações</h2>
                <InformacoesForm />
            </section>
        )
    }
}

export default Informacoes
