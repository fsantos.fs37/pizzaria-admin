import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CadastroForm from '../components/Form';
import styled from 'styled-components';

const ToPizzasCombo = styled.button`
    border: 1px solid #999;
    background: #ffffff;
    border-radius: 5px;
    padding: 10px 20px;
    cursor: pointer;
`

class Cadastrar extends Component {

    render() {
        return (
            <section className='admin-section'>
                <div className='section-title'>Cadastrar produto
                <div>
                        <Link to='/cadastrarCombo'>
                            <ToPizzasCombo>Cadastrar combo de pizzas</ToPizzasCombo>
                        </Link>
                    </div>
                </div>
                <CadastroForm />
            </section>
        )
    }
}

export default Cadastrar;
