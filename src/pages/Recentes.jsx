import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import PedidoCard from '../components/PedidoCard';

class Recentes extends Component {
    componentDidMount() {
        const { fetchPedidosRecentes, watchPedidos } = this.props.PedidosStore;
        watchPedidos();
        fetchPedidosRecentes('recentes', false);
    }

    render() {
        const { recentes } = this.props.PedidosStore;
        return (
            <section className='admin-section'>
                <h2 className='section-title'>Pedidos recentes</h2>
                <PedidoCard pedidos={recentes} />
            </section>
        )
    }
}

export default inject('PedidosStore')(observer(Recentes));

