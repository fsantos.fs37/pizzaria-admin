import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import PedidoCard from '../components/PedidoCard';

class Confrmados extends Component {
    componentDidMount() {
        const { fetchPedidosConfirmados, watchPedidos } = this.props.PedidosStore;
        watchPedidos();
        fetchPedidosConfirmados('confirmados', true);
    }

    render() {
        const { confirmados } = this.props.PedidosStore;
        return (
            <section className='admin-section'>
                <h2 className='section-title'>Pedidos confirmados</h2>
                <PedidoCard pedidos={confirmados} />
            </section>
        )
    }
}

export default inject('PedidosStore')(observer(Confrmados));

