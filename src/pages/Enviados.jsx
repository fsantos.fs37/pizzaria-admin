import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

import PedidoCard from '../components/PedidoCard';

class Recentes extends Component {
    componentDidMount() {
        const { fetchPedidosEnviados, watchPedidos } = this.props.PedidosStore;
        watchPedidos();
        fetchPedidosEnviados('recentes', false);
    }

    render() {
        const { enviados, deleteAllOrders } = this.props.PedidosStore;
        return (
            <section className='admin-section'>
                <h2 className='section-title'>Pedidos Enviados                <button className='button' onClick={deleteAllOrders}>Apagar todos os pedidos enviados</button>
                </h2>

                <PedidoCard pedidos={enviados} />
            </section>
        )
    }
}

export default inject('PedidosStore')(observer(Recentes));

