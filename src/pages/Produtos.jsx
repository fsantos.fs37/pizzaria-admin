import React, { Component } from 'react';

import ProdutosLista from '../components/ProdutosLista';

class Produtos extends Component {
    render() {
        return (
            <section className='admin-section'>
                <ProdutosLista />
            </section>
        )
    }
}

export default Produtos;
