import React, { Component } from 'react';
import CadastroComboForm from '../components/CadastroComboForm';

class CadastrarCombo extends Component {
    render() {
        return (
            <section className='admin-section'>
                <h2 className='section-title'>Cadastrar combo</h2>
                <CadastroComboForm isEdit={this.props.isEdit} />
            </section>
        )
    }
}

export default CadastrarCombo;
