import React, { Component } from "react"

import { storage, database } from "../firebase/firebase"

import { Progress, Form, ConfirmButton, FileInput, FileName } from '../StyledComponents/Form';
import { ImgContainer, Img } from '../StyledComponents/Image';

class CadastrarBanner extends Component {
    state = {
        file: null,
        progress: 0,
        disabled: false,
        currentBanner: null,
    }

    componentDidMount() {
        this.initialState = this.state;
        database.collection('promocoes').doc('banner').get()
            .then(doc => this.setState({ currentBanner: doc.data().bannerURL }))
    }

    sendFile = file => {
        const storageRef = storage.ref();
        const bannerRef = storageRef.child('banner.jpg');
        const uploadTask = bannerRef.put(file);

        uploadTask.on('state_changed', (snapshot) => {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            this.setState({ progress });
        }, error => {
            // Handle unsuccessful uploads
        }, _ => {
            alert('Banner atualizado com sucesso!')
            uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
                database.collection('promocoes').doc('banner').set({ bannerURL: downloadURL });
                this.setState({ ...this.initialState, currentBanner: downloadURL })
            });
        });
    }

    changeHandler = event => {
        this.setState({ file: event.target.files[0], disabled: false })
    }

    submitHandler = event => {
        event.preventDefault();
        const { file } = this.state
        console.log(this.state.file)
        if (file.type === 'image/png' || file.type === 'image/jpeg') {
            this.setState({ disabled: true })
            return this.sendFile(file)
        }
        return alert('Escolha um arquivo nos formatos .png ou .jpg!')
    }

    render() {
        return (
            <div>
                <ImgContainer>
                    <Img src={this.state.currentBanner} />
                </ImgContainer>
                <Form onSubmit={this.submitHandler}>
                    <Progress value={this.state.progress} max="100"></Progress>
                    <FileInput htmlfor="file">
                        {this.state.file ? <FileName>{this.state.file.name}</FileName>
                            : ' Escolher arquivo'}
                        <input type="file" name="file" onChange={this.changeHandler} />
                    </FileInput>
                    {this.state.file && <ConfirmButton disabled={this.state.disabled}>Enviar banner</ConfirmButton>}
                </Form>
            </div>
        )
    }
}

export default CadastrarBanner;