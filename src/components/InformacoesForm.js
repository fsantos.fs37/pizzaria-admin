import React, { Component, Fragment } from 'react';
import { database } from '../firebase/firebase';

class InformacoesForm extends Component {

    state = {
        horario: {
            inicio: 18,
            final: 0,
        },
        dias: []
    }

    componentDidMount() {
        this.initialState = this.state;
        database.collection('pizzaria').doc('horario').get()
            .then(response => {
                const data = response.data();
                this.setState({ ...data })
            })
    }

    changeHandler = event => {
        const { name, value } = event.target;
        const { horario } = this.state;
        horario[name] = value

        if (name === 'inicio' || name === 'final') {
            this.setState({ horario });
        }

        console.log(this.state)

        // const { dias } = this.state;
        // dias.push(value);
        // this.setState({ dias })
    }

    clickHandler = event => {
        const { value } = event.target;
        let { dias } = this.state;
        if (dias.includes(value)) {
            dias = dias.filter(item => item !== value)
            return this.setState({ dias })
        }
        dias.push(value);
        this.setState({ dias })
    }

    submitHandler = event => {
        event.preventDefault();
        event.target.disabled = true;
        const { horario, dias } = this.state;
        const produtosDB = database.collection('pizzaria');
        produtosDB.doc('horario').set({ horario, dias }).then(res => {
            // alert('Produto adicionado ao cardápio!');
            this.setState(this.initialState);
        })
    }

    render() {
        const diasDaSemana = ['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado', 'domingo'];

        const diasDaSemanaInputs = diasDaSemana.map(dia => (
            <label key={dia} className='dias-label'>
                <input value={dia} className='input' name={dia} type='checkbox' checked={this.state.dias.includes(dia)} onClick={this.clickHandler} onChange={_ => null} /> {dia}
            </label>
        ))

        return (
            <Fragment>
                <div className='informacoes-container'>
                    <form className='cadastro-form' onSubmit={this.submitHandler}>
                        <div className='cadastro-form-group'>
                            <label className='label horario' htmlFor='inicio'>
                                Abre às
                        <input className='input ' name='inicio' type='number' value={this.state.horario.inicio} onChange={this.changeHandler} required />
                            </label>
                            <label className='label horario' htmlFor='final'>
                                Fecha às
                        <input className='input' name='final' type='number' value={this.state.horario.final} onChange={this.changeHandler} required />
                            </label>
                        </div>
                        <div className='cadastro-form-group dias-container'>
                            <h3 className='dias-title'>Dias da Semana</h3>
                            <div className='dias'>
                                {diasDaSemanaInputs}
                            </div>
                        </div>
                        <button className='button cadastrar-produto-button' type='submit'>Cadastrar horário</button>
                    </form>
                </div>
            </Fragment>
        )
    }
}

export default InformacoesForm;
