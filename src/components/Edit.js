import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';

import { database } from '../firebase/firebase';

class Edit extends Component {

    state = {
        select: '',
        produto: {
            name: '',
            value: 0,
            ingredients: '',
            size: '',
            tipo: '',
        }
    }

    componentDidMount() {
        const { match } = this.props;
        const { tipo, id } = match.params;
        this.initialState = this.state;
        this.fetchProduct(tipo, id)
    }

    fetchProduct = (base, id) => {

        database.collection(base.toLowerCase()).doc(id).get().then(response => {
            const id = response.id;
            const { name, tipo, value, ingredients, size } = response.data();
            let { produto } = this.state;
            produto = { ...produto, name, tipo, value, ingredients, size };
            this.setState({
                select: base.toLowerCase(),
                produto

            })
        })
    }

    selectChangeHandler = event => {
        const { value } = event.target;
        if (!value) return;
        this.setState({ select: event.target.value });
    }

    changeHandler = event => {
        const { name, value } = event.target;
        if (!value) return;
        const state = this.state;
        state.produto[name] = value;
        this.setState({ state });
    }

    sanitizeData = obj => {
        const data = { ...obj };
        for (let prop in data) {
            if (data[prop].length < 1) {
                delete data[prop];
            }
        }
        return data
    }

    submitHandler = event => {
        event.preventDefault();
        event.target.setAttribute('disabled', true);
        const { id } = this.props.match.params;
        const { select, produto } = this.state;
        console.log(select, produto);
        const produtosDB = database.collection(`${select}`);
        produtosDB.doc(id).set(this.sanitizeData(produto)).then(_ => {
            alert('Produto atualizado com sucesso!');
            // window.location.pathname = '/produtos'
            // this.setState(this.initialState);
        })
    }

    render() {
        return (
            <form className='cadastro-form' onSubmit={this.submitHandler}>
                <div className='cadastro-form-group'>
                    <label className='label' htmlFor='select'>
                        Opção
                    <select className='select' value={this.state.select} onChange={this.selectChangeHandler} name='select' required>
                            <option>Selecionar</option>
                            <option value='pizzas'>Pizzas</option>
                            <option value='esfihas'>Esfihas</option>
                            <option value='bebidas'>Bebidas</option>
                            <option value='adicionais'>Adicionais</option>
                            <option value='bordas'>Bordas</option>
                        </select>
                    </label>
                    <label className='label' htmlFor='name'>
                        Nome
                    <input className='input' name='name' value={this.state.produto.name} onChange={this.changeHandler} required />
                    </label>
                    <label className='label preco' htmlFor='value'>
                        Preço
                    <input className='input' name='value' type='number' value={this.state.produto.value} onChange={this.changeHandler} required />
                    </label>
                </div>
                <div className='cadastro-form-group'>
                    {<Fragment>
                        <label className='label' htmlFor='tipo'>
                            Tipo
                            <select className='select' name='tipo' value={this.state.produto.tipo} onChange={this.changeHandler} required>
                                <option >Selecionar</option>
                                {this.state.select === 'bebidas'
                                    ? <Fragment>

                                        <option value='refrigerantes'>Refrigerantes</option>
                                        <option value='cervejas'>Cervejas</option>

                                    </Fragment>
                                    : <Fragment>
                                        <option value='salgadas'>Salgados</option>
                                        <option value='doces'>Doces</option>
                                        <option value='combo'>Combo</option>
                                        <option value='especiais'>Especiais</option>
                                    </Fragment>
                                }
                            </select>

                        </label>
                        {this.state.select === 'pizzas' || this.state.select === 'esfihas'
                            ? <Fragment>
                                <label className='label' htmlFor='size' key='textarea'>
                                    Tamanho
                            <select className='select' name='size' value={this.state.produto.size} onChange={this.changeHandler} required>
                                        <option >Selecionar</option>
                                        <option value='broto'>Broto</option>
                                        <option value='grande'>Grande</option>
                                    </select>
                                </label>
                                <label className='label textarea-label' htmlFor='ingredients' key='ingredients'>
                                    Ingredients
                                <textarea className='textarea' name='ingredients' rows='10' value={this.state.produto.ingredients} onChange={this.changeHandler} />
                                </label>

                            </Fragment>

                            : null

                        }
                    </Fragment>
                    }

                </div>
                <button className='button cadastrar-produto-button' type='submit'>Cadastrar produto</button>
            </form>
        )
    }
}

export default withRouter(Edit);
