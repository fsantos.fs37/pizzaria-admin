import React, { Component, Fragment } from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

import accounting from '../utils/accounting';

import { database } from '../firebase/firebase';

class PedidoCard extends Component {

    handleClick = id => {
        database.collection('pedidos').doc(id).set({ status: true, enviado: false }, { merge: true })
    }

    sendPedido = id => {
        database.collection('pedidos').doc(id).update({ enviado: true })
    }

    apagarPedido = id => {
        database.collection('pedidos').doc(id).delete().then(_ => {
            alert('Pedido apagado!')
            window.location.pathname = window.location.pathname;
        })
    }

    render() {
        const { pedidos, PedidosStore } = this.props;
        const { apagarPedido, sendPedido, watchPedidos } = PedidosStore;
        watchPedidos();
        const cards = pedidos ? pedidos.map(item => {
            const { order, payment, price, adress, status, enviado } = item;

            const pedidos = order.map((pedido, index) => {
                console.log(pedido.partsOptions)
                return (
                    <div className='pedido-card' key={pedido.name}>
                        <div className='card-title'>Item #{index + 1}</div>
                        <div className='row'>
                            Nome
                            <p className='name'> {pedido.name}</p>
                        </div>
                        <div className='row'>
                            Quantidade:<p> {pedido.counter}</p>
                        </div>
                        {pedido.partsOptions.length > 0
                            ? <ul className='pedido-adicionais'>
                                Partes
                                {pedido.partsOptions.map(adicional => <li className='row partes-adicionais' key={adicional.id}>{adicional.name}
                                    <ul>
                                        Adicionais para esta parte:
                                    {adicional.adicionais.map(item => (
                                            <li className='row' key={item.id}>{item.name}
                                                <p>{accounting.formatMoney(item.value)}</p>
                                            </li>
                                        ))}</ul>
                                </li>)}
                            </ul> : null}
                        <div className='row'>Valor:<p > {accounting.formatMoney(pedido.value)}</p></div>
                        {pedido.adicionais.length > 0
                            ? <ul className='pedido-adicionais'>
                                Adicionais
                                {pedido.adicionais.map(adicional => <li className='row' key={adicional.id}>{adicional.name}<p>{accounting.formatMoney(adicional.value)}</p></li>)}
                            </ul> : null}
                        Observação:
                            <div className='row'>
                            <p> {pedido.observacao}</p>
                        </div>
                    </div>)
            });

            const adressCard = adress => (
                <div className='pedido-card pedido-pagamento'>
                    <div className='card-title'>Endereço para entrega</div>
                    <div className='row'>Nome <p>{adress.nome.value}</p></div>
                    <div className='row'>Rua <p>{adress.rua.value}</p></div>
                    <div className='row'>N° <p>{adress.numero.value}</p></div>
                    <div className='row'>Bairro <p>{adress.bairro.value}</p></div>
                    <div className='row'>CEP <p>{adress.cep.value}</p></div>
                    <div className='row'>Complemento <p>{adress.complemento.value}</p></div>
                    <div className='row'>Telefone <p>{adress.tel.value}</p></div>
                </div>
            )
            return (
                <div className='order-card-container' key={item.id}>
                    <div className='row pedido-total'>
                        Total: <p>{accounting.formatMoney(price)}</p>
                    </div>
                    <div className='row pedido-pagamento__metodo'>
                        Método de pagamento: <p className='pedido-detalhe'>{payment.metodo}</p>
                    </div>
                    {payment.troco ? <div className='pedido-troco row'>Troco para: <p>{accounting.formatMoney(payment.troco)}</p></div> : null}
                    {pedidos}
                    {adressCard(adress)}
                    <div className='button-container'>
                        {!status && <button className='button confirm-button' onClick={() => this.handleClick(item.id)}>Confirmar pedido</button>
                        }
                        {status && !enviado && <button className='button confirm-button' onClick={() => this.sendPedido(item.id)}>Marcar como enviado</button>
                        }
                        {status && enviado && <button className='button confirm-button' onClick={() => this.apagarPedido(item.id)}>Apagar pedido</button>
                        }
                    </div>
                </div>
            )
        }) : []

        return <Fragment>{cards}</Fragment>

    }
}

export default inject('PedidosStore')(observer(PedidoCard));
