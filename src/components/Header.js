import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import { auth, signIn, signOut } from '../firebase/firebase';

import '../styles/Sidebar.css'

class Header extends Component {
    state = {
        user: false,
        openClass: false,
    }

    componentDidMount() {
        auth.onAuthStateChanged(user => {
            if (user) {
                this.setState({ user: true })
            }
        });

        const menuAnchors = [
            ...document.querySelector('.admin-header__nav').children,
            document.querySelector('.close'),
        ];
        menuAnchors.map(anchor => {
            anchor.addEventListener('click', this.toggleNav)
        })
    }

    toggleNav = () => {
        if (!this.state.openClass) {
            return this.setState({ openClass: 'nav--open' }, () => {
            })
        }
        this.setState({ openClass: false });
    }

    highLightNavItem = _ => {
        const path = window.location.pathname;
        const active = document.querySelector('.highlight');
        if (active) {
            active.classList.remove('highlight');
        }
        const nav = document.querySelector('.admin-header__nav');
        if (!nav) return;
        const anchors = [...nav.querySelectorAll('a')];
        const highLight = anchors.filter(anchor => anchor.getAttribute('href') === path);
        highLight[0].classList.add('highlight')
    }

    render() {
        console.log(this.state.openClass)

        return (
            <div className='admin-header-container'>
                <div className='admin-header'>
                    <div className='header-left-container'>
                        <button className='open' onClick={this.toggleNav}>
                            <i class="material-icons">menu</i>
                        </button>
                        <Link to='/'>Produtos</Link>
                    </div>
                    <div className={`admin-header__nav ${this.state.openClass}`}>
                        <button className='close'>
                            <i class="material-icons">close</i>
                        </button>
                        <Link to='/recentes'>Recentes</Link>
                        <Link to='/confirmados'>Confirmados</Link>
                        <Link to='/enviados'>Enviados</Link>
                        <Link to='/cadastrar'>Cadastrar</Link>
                        <Link to='/cadastrarCombo'>Cadastrar Combos</Link>
                        <Link to='/produtos'>Todos os produtos</Link>
                        <Link to='/cadastrarBanner'>Banners</Link>
                        <Link to='/promocoes'>Promocões</Link>
                        <Link to='/informacoes'>Informações</Link>
                        <Link to='/usuarios'>Usuários</Link>
                    </div>

                    <div className='header-login-area'>
                        {this.state.user
                            ? <span className='header-user-name'>{auth.currentUser.displayName}</span> : null}
                        {!this.state.user
                            ? <button className='button auth-button' onClick={signIn}>Entrar</button>
                            : <button className='button auth-button' onClick={signOut}>Sair</button>}

                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Header);
