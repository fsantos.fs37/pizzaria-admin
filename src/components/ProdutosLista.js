import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';

class ProdutosLista extends Component {
    state = { produtos: {} }

    componentDidMount() {
        const { getCombosCadastrados } = this.props.ProductsStore;
        getCombosCadastrados()
        this.getProducts();
    }

    componentWillReceiveProps() {
    }

    getProducts() {
        const { getProducts } = this.props.ProductsStore;
        const databases = ['adicionais', 'bebidas', 'bordas', 'esfihas', 'pizzas'];
        databases.map(item => getProducts(item))
    }

    render() {
        const { produtos, deleteProduct, combosCadastrados } = this.props.ProductsStore;
        const listaDeProdutos = [];
        const itemSection = (item, tipo) => {
            if (!item) return <div></div>;
            const cards = (
                <div className='produtos-column' key={Math.random() * 23}>
                    <h2>{tipo}</h2>
                    {Array.isArray(item) && item.map((produto, index) => (
                        produto.tipo !== 'pizzasCombo' && <div className='produto-card' key={produto.name + index}>
                            <div className='produto-card__title'>
                                <h2 className='produto-title'>{produto.name}</h2>
                            </div>
                            {produto.size
                                ? <p className='produto-text'>Tamanho: {produto.size}</p> : null}
                            {produto.ingredients
                                ? <p className='produto-text'>Ingredientes: {produto.ingredients}</p> : null}
                            <p className='produto-text'>Preço: {produto.value}</p>
                            <div className='produto-buttons-container'>
                                <button className='apagar-produto-button'
                                    onClick={() => deleteProduct(produto.id, produto.collection)}>
                                    <i className="material-icons">delete</i>
                                    Apagar produto
                        </button>
                                {<Link to={!produto.tipo ? `/editarCombo/${produto.id}` : `editar/${tipo}/${produto.id}`}><button className='editar-produto-button '>
                                    <i className="material-icons">edit</i>
                                    Editar produto
                        </button>
                                </Link>}
                            </div>
                        </div>
                    ))}
                </div>
            )

            listaDeProdutos.push(cards);
        }
        console.log(combosCadastrados)
        itemSection(combosCadastrados, 'Combos');
        itemSection(produtos.pizzas, 'Pizzas');
        itemSection(produtos.esfihas, 'Esfihas');
        itemSection(produtos.adicionais, 'Adicionais');
        itemSection(produtos.bordas, 'Bordas');
        itemSection(produtos.bebidas, 'Bebidas');

        return (
            <div className='produtos-container'>
                {listaDeProdutos.length > 4 && listaDeProdutos || 'Carregando produtos'}
            </div>
        )
    }
}

export default inject('ProductsStore')(observer(ProdutosLista));