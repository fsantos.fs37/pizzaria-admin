import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import styled from 'styled-components';
import accounting from '../utils/formatMoney';

import { database } from '../firebase/firebase';

const Container = styled.ul`
    list-style: none;
`

const Ul = styled.ul`
    list-style: none;
`

const Option = styled.li`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin: 0 0 10px;
    padding: 10px;
    min-width: 200px;
    border: ${props => props.selected ? '2px solid tomato' : '2px solid #999'};
    border-radius: 5px;
    background-color: #ffffff;
    cursor: pointer;
`

const NamePrice = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 10px
`
const Ingredients = styled.p`
    font-size: 12px;
    display: flex;
    justify-content: space-between;
`

const ListTitle = styled.h2`
    font-size: 18px;
`

const Label = styled.label`
    display: flex;
    flex-direction: column;
    margin: 0 0 10px;
`

const Form = styled.form`
    margin: 0 0 15px;
    align-self: flex-start;
    padding: 15px;
    border-radius: 3px;
    border: 2px solid #999;
    border-radius: 3px;
    background-color: #ffffff;
`

const Input = styled.input`
    max-width: ${props => props.large ? '100%' : '100px'};
    width: 100%
    padding: 10px;
    border-radius: 3px;
    border: 1px solid #999;
    margin: 10px 0 0;
`


class CadastroComboForm extends Component {
    state = {
        combo: [],
        selected: [],
        name: '',
        value: '',
        numeroSabores: 1,
        tipo: 'pizzasCombo',
    }

    inputs = new Set();


    edit = _ => {
        const { id } = this.props.match.params;
        if (id) {
            this.fetchcCombo(id)
        }
    }

    fetchcCombo = id => {
        database.collection('pizzas').doc(id).get()
            .then(response => {
                const data = response.data();

                const selected = data.combo.map(item => item.id);
                const combo = data.combo;
                const name = data.name;
                const value = data.value;
                const numeroSabores = data.numeroSabores;
                console.log(selected);
                this.setState({ selected, name, numeroSabores, value, combo },
                    () => console.log(this.state))
            })
    }

    componentDidMount() {
        const { getComboProducts } = this.props.ProductsStore;
        console.log(getComboProducts)
        getComboProducts();
        if (this.props.isEdit) {
            this.edit()
        }
    }

    handleChange = ({ target: { name, value } }) => {
        this.setState({ [name]: value });
    }

    selectHandler = produto => {
        const { selected, combo } = this.state;
        if (selected.includes(produto.id)) {
            selected.splice(selected.indexOf(produto.id), 1);
            combo.splice(combo.indexOf(produto));
            return this.setState({ selected, combo }, _ => console.log(this.state));
        }
        selected.push(produto.id);
        combo.push({ ...produto });
        this.setState({ selected, combo }, _ => console.log(this.state));
    }

    isValid = _ => {
        const { name, value, combo, numeroSabores } = this.state
        return name.length > 1 && value > 0 && combo.length > 1 && (numeroSabores > 0 && numeroSabores < 3);
    }

    cadastrarComboEditado = _ => {
        const { id } = this.props.match.params;
        if (!id) return;
        database.collection('pizzas').doc(id).set(this.formatCombo())
            .then(res => {
                alert('Produto atualizado com Sucesso')
            })
    }

    formatCombo = e => {
        if (e) e.preventDefault();
        const { name, value, combo, numeroSabores, tipo } = this.state;

        const newCombo = [...combo]
        const values = [...this.inputs].filter(Boolean)
            .filter(item => item.value > 1)
            .map(input => {
                return ({ id: input.name, value: input.value })
            });

        const maped = values.map(value => {
            return newCombo.map(item => {
                if (item.id === value.id)
                    return newCombo.splice(combo.indexOf(item), 1,
                        { ...item, quantidade: value.value }
                    )
                return item
            })
        })
        const obj = {
            name,
            value: Number.parseInt(value, 10),
            tipo,
            numeroSabores: Number.parseInt(numeroSabores),
            combo: newCombo
        };
        return obj

    }

    cadastrarCombo = e => {
        const { cadastrarCombo } = this.props.ProductsStore;
        if (!this.isValid()) return alert('Preencha todos os campos!');
        cadastrarCombo(this.formatCombo(e))
    }

    render() {
        const { ComboProducts: { pizzas, bebidas } } = this.props.ProductsStore;
        console.log(pizzas)

        const pizzasContainer = pizzas && pizzas.map(pizza => {
            return (
                pizza.tipo !== 'pizzasCombo' &&
                <Option
                    onClick={_ => this.selectHandler(pizza)}
                    selected={this.state.selected.includes(pizza.id)}
                    key={pizza.id}>
                    <NamePrice>
                        <span>{pizza.name}</span> <span>{accounting.formatMoney(pizza.value)}</span>
                    </NamePrice>
                    <NamePrice>
                        Tamanho: <span>{pizza.size}</span>
                    </NamePrice>
                    <Ingredients>{pizza.ingredients}</Ingredients>
                </Option >
            )
        })
        const bebidasContainer = bebidas && bebidas.map(refri => {
            return (
                <Option
                    onClick={_ => { this.selectHandler(refri) }}
                    selected={this.state.selected.includes(refri.id)}
                    key={refri.id}>
                    <NamePrice>
                        <span>{refri.name}</span><span>{accounting.formatMoney(refri.value)}</span>
                    </NamePrice>
                    <NamePrice>
                        Quantidade: <input style={{ maxWidth: 50 }} type='number' placeholder='quantidade' min={1} name={refri.id} ref={i => this.inputs.add(i)} onClick={e => e.stopPropagation()} defaultValue='1' />
                    </NamePrice>

                </Option >
            );

        })
        console.log(pizzas)
        return (
            pizzas && bebidas && <Container>
                <div style={{ display: 'flex' }}>
                    <Ul>
                        <ListTitle>Pizzas</ListTitle>
                        {pizzasContainer}
                    </Ul>
                    <Ul>
                        <ListTitle>Bebidas</ListTitle>

                        {bebidasContainer}
                    </Ul>
                    <div style={{ marginLeft: 40 }}>                    <ListTitle>Informações</ListTitle>
                        <Form onSubmit={e => e.preventDefault()}>
                            <Label >
                                Nome do combo:
                            <Input type='text' name='name' value={this.state.name} onChange={this.handleChange} required={true} large />
                            </Label>
                            <Label>
                                Valor do combo:
                            <Input type='number' name='value' value={this.state.value} onChange={this.handleChange} required={true} />
                            </Label>
                            <Label>
                                Número de sabores adicionais para este combo:
                            <Input type='number' max={3} min={1} name='numeroSabores' value={this.state.numeroSabores} onChange={this.handleChange} required={true} />
                                Máximo: 3
                        </Label>
                            {!this.props.isEdit
                                ? <button onClick={this.cadastrarCombo}>Cadastrar combo</button>
                                : <button onClick={this.cadastrarComboEditado}>Editar combo</button>
                            }
                        </Form>
                    </div>
                </div>
            </Container> || null
        )
    }
}

export default withRouter(inject('ProductsStore')(observer(CadastroComboForm)))
