import styled from "styled-components";

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 400px;
    margin: 10px auto;
`

export const Progress = styled.progress`
    -webkit-appearance: none;
    appearance: none;
    width: 90%;
    height: 10px;
    margin: 0 auto 0;
    border-radius: 10px;
    visibility: ${props => props.value > 0 ? 'visible' : 'hidden'}

    &::-webkit-progress-bar {
        background-color: #999;
        border-radius: 2px;
    }

    &::-webkit-progress-value {
        background-color: #1e88e5;
    }
`

export const Button = styled.button`
    border: 1px solid #999;
    background: transparent;
    padding: 10px 20px;
    margin: 10px auto;
    border-radius: 3px;
    color: #fff;
    border: none;
    background: #1e88e5;

`

export const ConfirmButton = styled.button`
    border: none;
    padding: 10px 20px;
    margin: 20px auto;
    border-radius: 3px;
    color: #fff;
    border: none;
    background: #1e88e5;
    padding: 10px 20px;
    pointer-events: ${props => props.disabled && 'none' || 'visible'};
`

export const FileInput = styled.label`
    display: flex;
    align-items: center;
    min-height: 45px;
    border: 1px solid rgba(0,0,0,.3);
    background: transparent;
    padding: 10px 20px;
    margin: 20px auto;
    border-radius: 3px;
    color: rgba(0,0,0,.8);
    padding: 10px 20px

    & input {
        position: absolute;
        opacity: 0;
    }
`

export const FileName = styled.span`
    font-size: 12px;
`
