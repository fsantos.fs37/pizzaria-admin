import styled from 'styled-components';

export const Container = styled.ul`
    list-style: none;
`

export const Ul = styled.ul`
    display: flex;
    flex-wrap: wrap;
    list-style: none;
    padding: 0;
    margin: 0 20px;

    & h2 {
        width: 100%;
    }

    & ul {
        padding: 0
    }

    & ul li {
        margin-right: 10px;
    }
`

export const Option = styled.li`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin: 0 0 10px;
    padding: 10px;
    min-width: 200px;
    border: ${props => props.selected ? '1px solid tomato' : ' 1px solid #dadce0'};
    border-radius: 5px;
    background-color: #ffffff;
    cursor: pointer;
`

export const NamePrice = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 10px
`
export const Ingredients = styled.p`
    font-size: 12px;
    display: flex;
    justify-content: space-between;
`

export const ListTitle = styled.h2`
    font-size: 18px;
`

export const Label = styled.label`
    display: flex;
    flex-direction: column;
    margin: 0 0 10px;
`

export const Form = styled.form`
    margin: 0 0 15px;
    align-self: flex-start;
    padding: 15px;
    border-radius: 3px;
    border: 2px solid #999;
    border-radius: 3px;
    background-color: #ffffff;
`

export const Input = styled.input`
    max-width: ${props => props.large ? '100%' : '100px'};
    width: 100%
    padding: 10px;
    border-radius: 3px;
    border: 1px solid #999;
    margin: 10px 0 0;
`

export const MainContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;

`

export const Button = styled.button`
border: none;
background: #ffffff;
padding: 10px 10px 10px;
display: flex;
justify-content: space-between;
align-items: center;
width: 145px;
margin-bottom: 10px;
border: 1px solid rgba(0, 0, 0, .1);
border-radius: 5px;
cursor: pointer;
color: #0288d1;
text-align: center;
margin: 20px 20px;
`