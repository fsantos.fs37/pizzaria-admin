import styled from "styled-components";


export const ImgContainer = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%
`
export const Img = styled.img`
    height: 300px;
    margin: 30px auto;
`