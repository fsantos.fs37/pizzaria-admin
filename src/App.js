import React, { Component, Fragment } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { observer, inject } from 'mobx-react';

import './App.css';

import Header from './components/Header'
import Home from './pages/Home'
import Recentes from './pages/Recentes.jsx'
import Confirmados from './pages/Confirmados'
import Cadastrar from './pages/Cadastrar';
import CadastrarCombo from './pages/CadastrarCombo';
import Produtos from './pages/Produtos';
import Informacoes from './pages/Informacoes';
import Enviados from './pages/Enviados';
import Editar from './pages/Editar';
import Promocoes from './pages/Promocoes';
import CadastrarBanner from './pages/CadastrarBanner';
import Usuarios from './pages/Usuarios';

class App extends Component {
  componentDidMount() {
    const { checkForUser } = this.props.UserStore;
    checkForUser();
  }

  render() {
    const { user } = this.props.UserStore;
    return (
      <BrowserRouter>
        <Fragment>
          <Header />
          <Switch>
            <Route exact path='/' component={user ? Recentes : Home} />
            <Route path='/recentes' component={user ? Recentes : null} />
            <Route path='/confirmados' component={user ? Confirmados : null} />
            <Route path='/cadastrar' component={user ? Cadastrar : null} />
            <Route path='/cadastrarCombo' component={user ? CadastrarCombo : null} />
            <Route path='/promocoes' component={user ? Promocoes : null} />
            <Route path='/editarCombo/:id' component={user ? () => <CadastrarCombo isEdit={true} /> : null} />
            <Route path='/produtos' component={user ? Produtos : null} />
            <Route path='/informacoes' component={user ? Informacoes : null} />
            <Route path='/enviados' component={user ? Enviados : null} />
            <Route path='/editar/:tipo/:id' component={user ? Editar : null} />
            <Route path='/cadastrarBanner' component={user ? CadastrarBanner : null} />
            <Route path='/usuarios' component={user ? Usuarios : null} />
          </Switch>
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default inject('UserStore')(observer(App));
