import { configure, action, observable, runInAction, decorate } from 'mobx';
import { database } from '../firebase/firebase';

configure({ enforceAction: 'observed' });

class ProductsStore {
    produtos = {};
    ComboProducts = {};
    combosCadastrados = [];

    getProducts = produto => {
        return database.collection(produto).get().then(response => {
            const data = [];
            response.docs.forEach(document => {
                data.push({ ...document.data(), id: document.id, collection: produto })
            });
            runInAction(() => this.produtos[produto] = data);
        });

    }

    getComboProducts = async _ => {
        const databases = ['bebidas', 'pizzas'];
        const produtos = {};
        for await (let produto of databases) {
            await database.collection(produto).get().then(response => {
                const data = [];
                response.docs.forEach(document => {
                    data.push({ ...document.data(), id: document.id, collection: produto })
                });
                produtos[produto] = data;
                console.log(data)
            });
        };
        runInAction(() => this.ComboProducts = produtos)
        localStorage.setItem('produtos', JSON.stringify(produtos))
    }

    getCombosCadastrados = () => {
        database.collection('pizzas').get()
            .then(response => {
                const data = response.docs.filter(item => {
                    console.log(item.data())
                    return item.data().tipo === 'pizzasCombo'
                }).map(document => {
                    const data = { ...document.data(), id: document.id, collection: 'pizzas' }
                    data.tipo = null;
                    return data;
                });

                runInAction(() => this.combosCadastrados = data);
            })
    }

    cadastrarCombo = produto => {
        database.collection('pizzas').add(produto)
            .then(res => {
                alert('Produto cadastrado com Sucesso')
            })
    }

    deleteProduct = (id, collection) => {
        database.collection(collection).doc(id).delete()
            .then(_ => alert('Produto deletado com sucesso'))
            .then(_ => {
                this.getProducts(collection)
            })
    }
}

decorate(ProductsStore, {
    produtos: observable,
    ComboProducts: observable,
    combosCadastrados: observable,
    getProducts: action,
    getComboProducts: action,
    cadastrarCombo: action,
    deleteProduct: action,
    getCombosCadastrados: action,
});

export default new ProductsStore();
