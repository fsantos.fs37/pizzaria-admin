import { configure, action, observable, runInAction, decorate } from 'mobx';
import { database } from '../firebase/firebase';

configure({ enforceAction: 'observed' });

class PromoStore {
    produtos = {};
    ComboProducts = {};
    promo = [];

    cadastrarPromo = promo => {
        const docRef = database.collection('promocoes').doc('pizzas');

        docRef.set(promo).then(res => {
            this.getPromo();
            alert('Cadastrado com sucesso!');
        })
    }

    getPromo = promo => {
        const docRef = database.collection('promocoes').doc('pizzas');

        docRef.get().then(doc => {
            runInAction(() => {
                this.promo = doc.data().combo;
            })
        })
    }

    getProducts = async _ => {
        if (Object.keys(this.produtos).length > 1) return;
        const databases = ['adicionais', 'bebidas', 'bordas', 'esfihas', 'pizzas'];
        const produtos = {};

        for await (let produto of databases) {
            await database.collection(produto).get().then(response => {
                const data = [];
                response.docs.forEach(document => {
                    data.push({ ...document.data(), id: document.id, collection: produto })
                });
                produtos[produto] = data;
                runInAction(() => this.produtos = produtos)
            });
        };

    }

}
decorate(PromoStore, {
    produtos: observable,
    ComboProducts: observable,
    promo: observable,
    getProducts: action,
    cadastrarPromo: action,
    getPromo: action,
});

export default new PromoStore();
