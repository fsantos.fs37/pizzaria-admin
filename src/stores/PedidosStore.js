import { configure, action, observable, runInAction, decorate } from 'mobx';
import { database } from '../firebase/firebase';

configure({ enforceAction: 'observed' });

class PedidosStore {
    recentes = [];
    confirmados = [];
    enviados = [];

    watchPedidos = _ => {
        const collection = database.collection('pedidos');
        collection.onSnapshot((snapshot) => {
            snapshot.docChanges().forEach((change) => {
                if (change.type === 'modified' || change.type === 'removed') {
                    this.fetchPedidosConfirmados('recentes', false);
                    this.fetchPedidosRecentes('confirmados', true);
                    this.fetchPedidosEnviados();
                }
            });
        })
    }

    fetchPedidosRecentes = async (estado, status) => {
        const collection = database.collection('pedidos');
        const response = await collection.where('status', '==', false).get();
        const data = [];
        response.forEach(doc => {
            const order = { id: doc.id, ...doc.data() }
            data.push(order)
        });
        runInAction(() => {
            this.recentes = data;
        });
    }

    fetchPedidosConfirmados = async (estado, status) => {
        const collection = database.collection('pedidos');
        const response = await collection.where('status', '==', true).where('enviado', '==', false).get();
        const data = [];
        response.forEach(doc => {
            const order = { id: doc.id, ...doc.data() }
            data.push(order)
        });
        console.log(data)
        runInAction(() => {
            this.confirmados = data;
        });
    }

    fetchPedidosEnviados = async _ => {
        const collection = database.collection('pedidos');
        const response = await collection.where('enviado', '==', true).get();
        const data = [];
        response.forEach(doc => {
            const order = { id: doc.id, ...doc.data() }
            data.push(order)
        });
        runInAction(() => {
            this.enviados = data;
        });
    }

    sendPedido = id => {
        database.collection('pedidos').doc(id).update({ enviado: true })
    }

    apagarPedido = id => {
        database.collection('pedidos').doc(id).delete().then(_ => {
            alert('Pedido apagado!')
        })
    }

    deleteAllOrders = async _ => {
        const collection = database.collection('pedidos');
        const response = await collection.where('enviado', '==', true).get();
        await response.forEach(doc => {
            collection.doc(doc.id).delete();
        });
    }
}

decorate(PedidosStore, {
    recentes: observable,
    confirmados: observable,
    fetchPedidosRecentes: action,
    fetchPedidosConfirmados: action,
    watchPedidos: action,
    sendPedido: action,
    apagarPedido: action,
    fetchPedidosEnviados: action,
    deleteAllOrders: action,
});

export default new PedidosStore();
