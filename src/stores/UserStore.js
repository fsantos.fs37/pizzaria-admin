import { configure, action, observable, runInAction, decorate } from 'mobx';
import { auth, database } from '../firebase/firebase';

configure({ enforceAction: 'observed' });

class UserStore {
    user = false;
    userInfo = {};
    users = [];

    checkForUser = _ => {
        auth.onAuthStateChanged(async user => {
            if (user) {
                const document = await database.collection('admin').doc(user.uid).get();
                if (!document.exists) return;
                const data = document.data();
                if (data.admin === true) {
                    runInAction(() => {
                        this.user = true;
                        this.userInfo = data;
                    });
                }
            }
        })
    }

    getUsers = _ => {
        database.collection('users').get().then(res => {
            const users = res.docs.map(doc => {
                console.log(doc.data())
                return doc.data();
            });
            runInAction(() => this.users = users);
        })
    }
}

decorate(UserStore, {
    user: observable,
    users: observable,
    userInfo: observable,
    checkForUser: action,
});

export default new UserStore();
