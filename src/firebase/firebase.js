import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import 'firebase/auth';

firebase.initializeApp({
    apiKey: "AIzaSyCp9QzLbkbT6MUwU807kr_R2gf_Y3-Rssk",
    authDomain: "notas-365.firebaseapp.com",
    databaseURL: "https://notas-365.firebaseio.com",
    projectId: "notas-365",
    storageBucket: "notas-365.appspot.com",
    messagingSenderId: "740723294888"
});

const firestore = firebase.firestore();
firestore.settings({ timestampsInSnapshots: true });

export const database = firestore;
export const storage = firebase.storage();
export const auth = firebase.auth();
export const signIn = _ => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            const adminDb = database.collection('admin');
            adminDb.doc(user.uid).get()
                .then(document => {
                    console.log(document.exists)
                    if (document.exists) return;

                    const { displayName, uid, photoURL, email } = user;

                    const data = { displayName, uid, photoURL, email, admin: false };

                    adminDb.doc(uid).set(data)
                });
        }
    })
}
export const signOut = _ => {
    firebase.auth().signOut();
    return window.location.pathname = '/';
}
