import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'mobx-react';
import UserStore from './stores/UserStore';
import ProductsStore from './stores/ProductsStore';
import PedidosStore from './stores/PedidosStore';
import PromoStore from './stores/PromoStore';

const stores = {
    UserStore,
    ProductsStore,
    PedidosStore,
    PromoStore,
}

const Root = (
    <Provider {...stores}>
        <App />
    </Provider>
)

ReactDOM.render(Root, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
